import api from "../Utils/api";
import { queryString } from "../Utils/helper";

export const search = payload =>
  api.get(`/movie-search?${queryString(payload)}`);

export default {
  search,
};
