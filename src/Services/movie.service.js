import api from "../Utils/api";

export const getMovies = () => api.get(`/movie`);
export const getMovie = ({ id }) => api.get(`/movie/${id}`);

export default {
  getMovies,
  getMovie,
};
