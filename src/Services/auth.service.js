import api from "../Utils/api";

export const getToken = () => api.post(`/token`);

export default {
  getToken,
};
