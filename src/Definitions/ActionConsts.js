export default {
  Search: {
    isSearchLoading: "IS_SEARCH_LOADING",
    searchMovieList: "SEARCH_MOVIE_LIST",
    movieSearchDone: "MOVIE_SEARCH_DONE",
    searchReset: "SEARCH_RESET",
    searchError: "SEARCH_ERROR",
  },
};
