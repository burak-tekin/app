import { connect } from "react-redux";
import SearchMovieList from "./SearchMovieList";

const SearchResult = ({ isSearchDone, isLoading, movieList, error }) => {
  const renderResult = () => {
    if (error) return <div className="result-info">There is a problem</div>;

    return (
      <>
        <div className="result-info">{movieList.length} results found</div>
        <SearchMovieList list={movieList} />
      </>
    );
  };

  return (
    <div className="result">
      {isSearchDone && !isLoading ? renderResult() : <></>}
    </div>
  );
};

const mapStateToProps = state => ({
  movieList: state.search.movieList,
  isSearchDone: state.search.isSearchDone,
  error: state.search.error,
  isLoading: state.search.isLoading,
});

export default connect(mapStateToProps)(SearchResult);
