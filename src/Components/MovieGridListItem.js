import Link from "next/link";
import ReactStars from "react-rating-stars-component";
import Category from "./Category";

const MovieGridListItem = ({ movie }) => (
  <div className="movie-list-item-wrapper">
    <div className="movie-list-item">
      <Link href={`/film/${movie.id}`}>
        <a className="cover">
          <img src={movie.mediumImage} alt="" />
        </a>
      </Link>
      <div className="detail">
        <Link href={`/film/${movie.id}`}>
          <a className="title">{movie.name}</a>
        </Link>
        <p dangerouslySetInnerHTML={{ __html: movie.summary }} />
        {movie.rating && (
          <div className="ratings">
            <ReactStars
              count={10}
              size={15}
              value={movie.rating}
              edit={false}
              activeColor="#ffd700"
            />
            <span>{movie.rating}/10</span>
          </div>
        )}
        <div className="categories">
          {Array.isArray(movie.genres) &&
            movie.genres.map((category, index) => (
              <Category key={index}>{category}</Category>
            ))}
        </div>
      </div>
    </div>
  </div>
);

export default MovieGridListItem;
