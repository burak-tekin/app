import { useState, useCallback } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch, faSpinner } from "@fortawesome/free-solid-svg-icons";
import { connect } from "react-redux";
import debounce from "lodash.debounce";
import { searchMovieList } from "../Actions/search.action";

const SearchForm = ({ isLoading, searchMovie }) => {
  const [query, setQuery] = useState("");

  const search = useCallback(
    debounce(newQuery => {
      if (!isLoading && newQuery.length >= 3) {
        searchMovie({ query: newQuery });
      }
    }, 600),
    []
  );

  const submit = e => {
    e.preventDefault();
    search(query);
  };

  const changeQueryInput = e => {
    setQuery(e.target.value);
    search(e.target.value);
  };

  return (
    <form className="search-input" onSubmit={submit}>
      <input
        type="text"
        value={query}
        onChange={changeQueryInput}
        placeholder="Search Movie"
      />
      <button>
        <FontAwesomeIcon icon={isLoading ? faSpinner : faSearch} />
      </button>
    </form>
  );
};

const mapStateToProps = state => ({
  isLoading: state.search.isLoading,
});

const mapDispatchToProps = {
  searchMovie: searchMovieList,
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchForm);
