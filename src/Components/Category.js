const Category = ({ children }) => <span className="category">{children}</span>;
export default Category;
