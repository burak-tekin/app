import { useState, useRef } from "react";
import { connect } from "react-redux";
import SearchResult from "./SearchResult";
import SearchForm from "./SearchForm";
import { reset as resetAction } from "../Actions/search.action";

const Search = ({ reset, isSearchDone, isLoading }) => {
  const [isSearchFocus, setSearchFocus] = useState(false);
  const searchWrapper = useRef();

  const handleBlur = () => {
    setTimeout(() => {
      setSearchFocus(false);
      reset();
    }, 250);
  };

  const handleFocus = () => {
    reset();
    setSearchFocus(true);
  };

  return (
    <div
      className="search-wrapper"
      ref={searchWrapper}
      onFocus={handleFocus}
      onBlur={handleBlur}
    >
      <div
        className={`search ${
          isSearchFocus && isSearchDone && !isLoading && "searched"
        }`}
      >
        <SearchForm />
        <SearchResult />
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  isSearchDone: state.search.isSearchDone,
  isLoading: state.search.isLoading,
});

const mapDispatchToProps = {
  reset: resetAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
