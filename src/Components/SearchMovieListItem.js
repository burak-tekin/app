import ReactStars from "react-rating-stars-component";
import { useRouter } from "next/router";
import Category from "./Category";

const SearchMovieListItem = ({ movie }) => {
  const router = useRouter();
  const goDetail = e => {
    e.preventDefault();
    router.push(`/film/${movie.id}`);
  };
  return (
    <li className="search-movie-list_item">
      <a className="cover" onClick={goDetail}>
        <img src={movie.mediumImage} alt="" />
      </a>
      <div className="detail">
        <a className="title" onClick={goDetail}>
          {movie.name}
        </a>
        <p dangerouslySetInnerHTML={{ __html: movie.summary }} />
        {movie.rating && (
          <div className="ratings">
            <ReactStars
              count={10}
              size={15}
              value={movie.rating}
              edit={false}
              activeColor="#ffd700"
            />
            <span>{movie.rating}/10</span>
          </div>
        )}
        <div className="categories">
          {Array.isArray(movie.genres) &&
            movie.genres.map((category, index) => (
              <Category key={index}>{category}</Category>
            ))}
        </div>
      </div>
    </li>
  );
};

export default SearchMovieListItem;
