import SearchMovieListItem from "./SearchMovieListItem";

const SearchMovieList = ({ list }) =>
  list && list.length ? (
    <ul>
      {list.map(movie => (
        <SearchMovieListItem
          movie={movie}
          key={`SearchMovieListItem-${movie.id}`}
        />
      ))}
    </ul>
  ) : (
    <></>
  );
export default SearchMovieList;
