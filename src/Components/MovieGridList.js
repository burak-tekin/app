import MovieGridListItem from "./MovieGridListItem";

const MovieGridList = ({ list }) =>
  list && list.length ? (
    <div className="movie-list">
      {list.map(movie => (
        <MovieGridListItem
          movie={movie}
          key={`MovieGridListItem-${movie.id}`}
        />
      ))}
    </div>
  ) : (
    <></>
  );
export default MovieGridList;
