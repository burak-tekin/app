import ActionConsts from "../../Definitions/ActionConsts";

const INITIAL_STATE = {
  isLoading: false,
  movieList: [],
  isSearchDone: false,
  error: false,
};
export const SearchReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ActionConsts.Search.isSearchLoading:
    case ActionConsts.Search.searchMovieList:
    case ActionConsts.Search.movieSearchDone:
    case ActionConsts.Search.searchError:
      return {
        ...state,
        ...action.payload,
      };
    case ActionConsts.Search.searchReset:
      return INITIAL_STATE;
    default:
      return state;
  }
};
