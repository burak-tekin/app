import ActionConsts from "../Definitions/ActionConsts";
import { search as searchRequest } from "../Services/search.service";

export const loading = (isLoading = false) => ({
  payload: { isLoading },
  type: ActionConsts.Search.isSearchLoading,
});

export const done = (isSearchDone = false) => ({
  payload: { isSearchDone },
  type: ActionConsts.Search.movieSearchDone,
});

export const setMovieList = payload => ({
  payload: { movieList: payload },
  type: ActionConsts.Search.searchMovieList,
});

export const reset = () => ({
  type: ActionConsts.Search.searchReset,
});

export const searchMovieList = payload => dispatch => {
  dispatch(loading(true));
  dispatch(done(false));
  searchRequest(payload)
    .then(res => {
      dispatch(setMovieList(res.data));
      dispatch(done(true));
    })
    .catch(() => {
      dispatch({
        type: ActionConsts.Search.searchError,
      });
    })
    .finally(() => {
      dispatch(loading(false));
    });
};
