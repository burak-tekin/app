import { useEffect, useState } from "react";
import "@fortawesome/fontawesome-svg-core/styles.css"; // import Font Awesome CSS
import { config } from "@fortawesome/fontawesome-svg-core";
import "../styles/main.sass";

import Head from "next/head";
import withRedux from "next-redux-wrapper";
import { makeStore } from "../src/Redux/store";
import { setToken } from "../src/Utils/api";
import { getToken } from "../src/Services/auth.service";

config.autoAddCss = false;

const MyApp = ({ Component, pageProps }) => {
  const [isTokenTaked, setTokenTaked] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem("token") || null;
    if (token) {
      setToken(token);
      setTokenTaked(true);
    } else {
      getToken().then(res => {
        setToken(res.data.token);
        setTokenTaked(true);
      });
    }
  }, []);

  return (
    <>
      <Head>
        <title>Movie</title>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
        />
      </Head>
      {isTokenTaked && <Component {...pageProps} />}
    </>
  );
};

export default withRedux(makeStore)(MyApp);
