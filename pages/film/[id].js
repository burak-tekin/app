import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import ReactStars from "react-rating-stars-component";
import { getMovie } from "../../src/Services/movie.service";
import Category from "../../src/Components/Category";

const MovieDetail = () => {
  const [movie, setMovie] = useState(null);
  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    getMovie({ id })
      .then(res => {
        setMovie(res.data);
      })
      .catch(() => {
        router.push("/");
      });
  }, []);

  if (movie && movie.id)
    return (
      <div className="movieDetail">
        <Link href="/">
          <a className="backToHome">
            <FontAwesomeIcon icon={faArrowLeft} />
            <span>Back To Home</span>
          </a>
        </Link>
        <div className="banner">
          <img src={movie.image} alt="" />
        </div>
        <div className="container movie-content">
          <div className="cover">
            <img src={movie.image} alt="" />
          </div>
          <div className="content">
            <span className="title">{movie.name}</span>
            {movie.rating && (
              <div className="ratings">
                <ReactStars
                  count={10}
                  size={15}
                  value={movie.rating}
                  edit={false}
                  activeColor="#ffd700"
                />
                <span>{movie.rating}/10</span>
              </div>
            )}
            <p dangerouslySetInnerHTML={{ __html: movie.summary }} />
            <div className="categories">
              {Array.isArray(movie.genres) &&
                movie.genres.map((category, index) => (
                  <Category key={index}>{category}</Category>
                ))}
            </div>
          </div>
        </div>
      </div>
    );
  return <></>;
};

export default MovieDetail;
