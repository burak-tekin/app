import { useEffect, useState } from "react";
import Search from "../src/Components/Search";
import { getMovies } from "../src/Services/movie.service";
import MovieGridList from "../src/Components/MovieGridList";

export default function Home() {
  const [movies, setMovies] = useState([]);
  useEffect(() => {
    getMovies().then(res => {
      setMovies(res.data);
    });
  }, []);
  return (
    <>
      <div className="hero-header">
        <h1 className="title">Search Movie</h1>
        <Search />
      </div>
      <div className="container home-content">
        <h2>Last Added Movies</h2>
        <div className="movie-list">
          <MovieGridList list={movies} />
          <div className="clearfix" />
        </div>
      </div>
    </>
  );
}
